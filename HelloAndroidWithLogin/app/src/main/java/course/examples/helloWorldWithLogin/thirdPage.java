package course.examples.helloWorldWithLogin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class thirdPage extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thirdpage);

        final Button btnBack = (Button) findViewById(R.id.btnBack);
        final Button btnHome = (Button) findViewById(R.id.btnHome);

        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent backPage = new Intent(view.getContext(), HelloAndroid.class);
                startActivity(backPage);
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent homePage = new Intent(view.getContext(), LoginScreen.class);
                startActivity(homePage);
            }
        });
    }
}
