package course.examples.helloWorldWithLogin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class HelloAndroid extends Activity {
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.helloandroid);

        final TextView textView = (TextView) findViewById(R.id.textView);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            String greet = extras.getString("UserName");
            textView.setText(textView.getText()+ " " + greet);
        }


        final Button NextButton = (Button) findViewById(R.id.button);
        NextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent NextPage = new Intent(view.getContext(), thirdPage.class);
                finish();
                startActivity(NextPage);
            }
        });
	}
}